/******************************************************************************
 * /CertTool/
 * Build and Use a Certificate Chain including RootCA and Intermediates for use
 * in BUILD.
 * Inspired by Jamie Nuygen's tutorial.
 * https://jamielinux.com/docs/openssl-certificate-authority/
 * 
 * This directory contains 
 * /bin - executbles, and
 * /cnf fir openssl configuration file used to generate a certificate chain
 * and root CA certificate for use with BUILD-FIPS/CC.
 *
 * The following are top-level targets implemented by the Makefile:
 *
 *     File 		        Description
 *     -----------          -----------
 *
 *  /intermediateX/BUILD/     BUILD-format server certificate chain including the
 *  .*$F_EXT               server and intermediate certificates, and ending
 *                          with the server certificate's private key
 *
 *  /BUILD/$CRT_FILE       BUILD-format public certificate file including the
 *                          root CA certificate
 *
 *  /certs/ca-cert.pem 	    Special mention: install this to your favourite web
 *                          browser in order to have it trust BUILD units you
 *                          provision with certificates authorized by it.
 *
 ******************************************************************************
#############################################################################
#
# Usage of tool assumes that the following utilities are available:
# 
#   - openssl
#	- sed
#
# (c) 2016 Moninder Gill MCorp 
#
#############################################################################
Understanding config files used:
 BASE_ROOT_CONF="../cnf/root_openssl.cnf"
 BASE_INT_CONF="../cnf/int_openssl.cnf"
 
 Note: Above config files are used in first step only and with '1.create_root_intermediate_hierarchy.sh'when creating a base chain. Subsequent operations will use config files from its directory.
 e.g CA_DIR/openssl.cnf, and
	 CA_DIR/intermediate[x]/openssl.cnf
 
 SIGNEE	 	SIGNER 		CONFIG_FILE_USED
 -----------------------------------------
 Int1 		 Root 		 CA_DIR/openssl.cnf
 IntY 		 Int[x] 	 CA_DIR/intermediate[x]/openssl.cnf
 Int3 		 Root 		 CA_DIR/openssl.cnf
 
 Leaf1 		 Int[x]	 	 CA_DIR/intermediate[x]/openssl.cnf
 Leaf2 		 Root 		 CA_DIR/openssl.cnf
-------------------------------------------------------------------------------------------------------------
1.create_root_intermediate_hierarchy.sh
-------------------------------------------------------------------------------------------------------------
- Creates/Revokes Root and Intermediate certificate hierarchy.
	./1.create_root_intermediate.sh [role] [key_type] [intermediate] [intermediate_signer]
	
	#Creates a RootCA with RSA.
	./1.create_root_intermediate.sh root rsa 
	
	#Creates Intermediate1 with RSA pair which is signed by RootCA
	./1.create_root_intermediate.sh int rsa 1
	#Creates Intermediate2 with RSA pair which is signed by RootCA
	./1.create_root_intermediate.sh int rsa 2
	
	./1.create_root_intermediate.sh int rsa 2 1
	#Creates Intermediate2 with RSA pair which is signed by Intermediate1

- Revokes Intermediate Certificate
	#Revokes Intermediate2 which was signed by Intermediate1
	./1.create_root_intermediate.sh rev 2 1
	#Revokes Intermediate1 which was signed by RootCA
	./1.create_root_intermediate.sh rev 1 r

-------------------------------------------------------------------------------------------------------------
2.gen_leaf_certificates.sh [config_file]
-------------------------------------------------------------------------------------------------------------
- Reads from a text_config file (one.twos) line by line and generates leaf/OCSP certificates.
- Format of config file.

[cert_arguments] [signer_number] [hashing_algo] [keysize] [type/action]

[hashing_algo] - sha256, sha512, sha384 etc
[keysize] - For RSA (1024, 2048, 3072, 4096), For ECC(256, ,384, 521)
[type/action]
	C - Create a Leaf Certificate ([server_cert] in config file)
	S - Skip a line, move to next.
	R - Revoke a Leaf certificate.
	O - Create an OCSP signer certificate.

Once certificates are created, user can replace last character with 'S' to skip Certificate line from not being processed repeatedly.
--------------------------------------------------------------------------------------------------
E.g. OCSP certificate signed by Intermediate1.
/C=CA/ST=ON/O=MCorp-Canada/OU=MCorp-CA-Authority/CN=ocsp1.local/emailAddress=oscp@example.local/ 1 sha256 2048 O

E.g. Leaf certificate signed by RootCA.
/C=CA/ST=ON/O=MCP-TEST/OU=MCP-TEST-Lab/CN=test1.example.local/emailAddress=t1@r/ 0 sha256 2048 C

E.g. Leaf certificate signed by Intermediate1.
/C=CA/ST=ON/O=MCP-TEST/OU=MCP-TEST-Lab/CN=test1.example.local/emailAddress=t1@r/ 1 sha256 2048 C

E.g. Leaf certificate signed by Intermediate2.
/C=CA/ST=ON/O=MCP-TEST/OU=MCP-TEST-Lab/CN=test2.example.local/emailAddress=t1@r/ 2 sha256 2048 C

-------------------------------------------------------------------------------------------------------------
3.launch_OCSP_responder_and_client.sh
-------------------------------------------------------------------------------------------------------------
- Launches OCSP responder to query results from. Client can be initiated by same script or by BUILD.

 Currently OCSP script works with predefined name parameters
 ./3.launch_OCSP_responder_and_client.sh [server] [int/leaf] [signing_intermediate] [port]
 ./3.launch_OCSP_responder_and_client.sh [client] [int/leaf] [signing_intermediate] [port] [test_certificate]
 ./3.launch_OCSP_responder_and_client.sh [answer] [int/leaf] [number] [port] 


If [test_certificate] not specified then script reads default value.

	./3.launch_OCSP_responder_and_client.sh server 2 2560
	./3.launch_OCSP_responder_and_client.sh client 2 2560 

- Clean up files log files from previous responders.
  ./3.launch_OCSP_responder_and_client.sh cleanup

- Run script in intuitive mode easy to read.
  ./3.launch_OCSP_responder_and_client.sh answer int 1 2561

-------------------------------------------------------------------------------------------------------------
4.sign_csr_with_rsa_or_ec_cert.sh
-------------------------------------------------------------------------------------------------------------
- Signs a given csr and adds entry to Root index file. 
	A very simple hierarchy is used
	
	RootCA -> signed_csr_cert.pem

-------------------------------------------------------------------------------------------------------------
5.check_serial.sh
6.find_length.sh
These two basic scripts are just for quick checks (checking serial and CA:length for given certs)
-------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------
7.send-one-by-one.sh [$HOST]
7.send-one-by-one.sh [$HOST] [s/leaf_no]
-------------------------------------------------------------------------------------------------------------

	s -> $CRT_FILE 
	leaf_no -> for end leaf certificate

If Ran with a SECOND argument, it'll only send specified certificate.
If Ran with only ONE argument, it'll all files ($CRT_FILE and upto 3 leafs) one by one

-------------------------------------------------------------------------------------------------------------
8.auto-detect-nodes.sh
-------------------------------------------------------------------------------------------------------------
Wrapper to "3.launch_OCSP_responder_and_client.sh" that automaticallly reads OSCP port info from given certificate.
 ./8.auto-detect-nodes.sh int 1  # Reads /intermediate1/certs/intermediate1.cert.pem, launches OCSP responder.
 ./8.auto-detect-nodes.sh int 2  # Reads /intermediate1/certs/intermediate2.cert.pem, launches OCSP responder.
 ./8.auto-detect-nodes.sh leaf 1 # Reads /intermediate1/system/test1*$F_EXT, launches OCSP responder.
 ./8.auto-detect-nodes.sh leaf 2 # Reads /intermediate1/system/test1*$F_EXT, launches OCSP responder.

 Check running launchers
 $ps aux | grep ocsp
-------------------------------------------------------------------------------------------------------------
9.verify_cert_chain_with_Openssl.sh
-------------------------------------------------------------------------------------------------------------
Just checks validity of certificate Chain using Openssl before provisioning to a device. Sanity Check.
SYNTAX: 9.verify_cert_chain_with_Openssl.sh [length_of_chain]
	9.verify_cert_chain_with_Openssl.sh 8
	
	DEBUG: Testing for Leaf1> k/root/ca_chain.rsa/intermediate1/certs/test1.example.local-2048-sha256-signedbyINT1.cert.pem: OK
	DEBUG: Testing for Leaf2> k/root/ca_chain.rsa/intermediate2/certs/test2.example.local-2048-sha256-signedbyINT2.cert.pem: OK



-------------------------------------------------------------------------------------------------------------
Pending
-------------------------------------------------------------------------------------------------------------
#Create certificate hierarchy with Root -> Intermediate1 -> Intermediate2

./1.create_root_intermediate.sh root rsa
./1.create_root_intermediate.sh int rsa 1
./1.create_root_intermediate.sh int rsa 2 1


#Create Leaf1/2 and OCSP1/2 from Intermediate 1 and Intermediate2.
root@mgdeb:~# ./2.function_genCertificates.sh one.twos 
[one.twos]
/C=CA/ST=ON/O=MCorp-Canada/OU=MCorp-CA-Authority/CN=ocsp1.local/emailAddress=oscp@ex.local/ 1 sha256 2048 C
/C=CA/ST=ON/O=MCP-TEST/OU=MCP-TEST-Lab/CN=test1.example.local/emailAddress=t1@r/ 1 sha256 2048 C

/C=CA/ST=ON/O=MCorp-Canada/OU=MCorp-CA-Authority/CN=ocsp2.local/emailAddress=oscp@ex.local/ 2 sha256 2048 C
/C=CA/ST=ON/O=MCP-TEST/OU=MCP-TEST-Lab/CN=test2.example.local/emailAddress=t1@r/ 2 sha256 2048 C