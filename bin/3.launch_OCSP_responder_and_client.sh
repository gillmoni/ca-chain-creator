#!/bin/bash
#C_DEBUG="echo"
source ../cnf/ca_settings.txt

DIR="$CA_DIR/intermediate$SIGNER_NO"
################################################################################################################################
ROLE=$1 ; CERT_TYPE=$2 ; SIGNER_NO=$3 ; PORT=$4 ; END_CERT_TO_CHECK=$5
URL="$PC_IP:$PORT"
#URL="127.0.0.1:$PORT"

if [[ $ROLE == "cleanup" || $ROLE == "c" ]]; then #echo "Cleanup logs it created. 
	rm ocsp-leaf* ocsp-int* > /dev/null ; did_it_work delete_old_ocsp_logs;  
	exit
fi

######################################################################################################
#Check if external OCSP responder exits.
######################################################################################################
if [[ $OCSP_RESPONDER == 'external' &&  ! -d $OCSP_CA_DIR ]]; then
  echo -e "Launcher cannot operate. External OCSP Responder <$OCSP_CA_DIR/> doesn't exist. Exiting!!" ; exit ; enter2continue
fi
######################################################################################################
#IF_RUNNING_EVERYTHING
######################################################################################################
if [[ ! $ROLE || ! $CERT_TYPE || ! $SIGNER_NO || ! $PORT ]]; then echo "Not enough arguments. Exit!" ; echo "script: [client/server/answer] [lead/int] [SIGNER_NO] [port] [certificate]"; exit ; fi
######################################################################################################
#SERVER
######################################################################################################
#Read OCSP_RESPONDER value from ca_settings.txt
#case "$OCSP_RESPONDER" in
	#"internal")	#echo -n "DEBUG: Using $OCSP_RESPONDER OCSP responder> " ; enter2continue 
				#ROLE='server' ;
	#;;
	#"external")	#echo -n "DEBUG: Using $OCSP_RESPONDER OCSP responder> " ; enter2continue 
				#ROLE='responder' ;
	#;;
	#*)			echo "Invalid Arguments> [OCSP_RESPONDER internal/external]" ; exit ;
	#;;
 #esac


if [[ $ROLE == "server" || $ROLE == "responder" ]] ; then #echo "Running in Server Mode" 
	#echo -n "DEBUG: Acting as a $ROLE sever> "; enter2continue
	#------------------------------------------------------------------------------------------------
	#ASSIGN PARAMETERS| Root -> Int1 | Int[x-1]->Int[x]
	#------------------------------------------------------------------------------------------------
	WORKER_NO=$SIGNER_NO 
	if [[ $CERT_TYPE == "int" ]]; then 
		DIR="$CA_DIR"
		if [[ $SIGNER_NO == "1" ]]; then
			#echo -n "DEBUG: OMG What are you doing!" ; enter2continue
			cert_index="$CA_DIR/index.txt"
			root_ca_cert=$(find $CA_DIR/ | grep ca.cert.pem)
			
			#cert_chain=$(find $CA_DIR/ | grep ca.cert.pem)
			cert_chain="$CA_DIR/certs/ca.cert.pem"
			cert_issuer="$CA_DIR/certs/ca.cert.pem"
			
			((SIGNER_NO  --))
			ocsp_cert=$(find $CA_DIR/certs/ | grep ocsp | grep cert.pem)
			ocsp_priv_key=$(find $CA_DIR/private/ | grep ocsp | grep key.pem)
	
		else
			#echo "DEBUG: Old SIGNER_NO > $SIGNER_NO"
			((SIGNER_NO  --))
 			root_ca_cert=$(find $CA_DIR/ | grep ca.cert.pem)
			cert_chain=$(find $CA_DIR/intermediate$SIGNER_NO/ | grep chain.cert.pem)
			cert_index="$CA_DIR/intermediate$SIGNER_NO/index.txt"
	
			cert_issuer="$CA_DIR/intermediate$SIGNER_NO/certs/intermediate$SIGNER_NO.cert.pem"
			
			ocsp_cert=$(find $CA_DIR/intermediate$SIGNER_NO/certs/ | grep ocsp | grep cert.pem)
			ocsp_priv_key=$(find $CA_DIR/intermediate$SIGNER_NO/private/ | grep ocsp | grep key.pem)
			
			#echo "DEBUG: New SIGNER_NO > $SIGNER_NO"
		fi

	#------------------------------------------------------------------------------------------------
	#ASSIGN PARAMETERS| Int[x]-> Leaf[x]
	#------------------------------------------------------------------------------------------------
	elif [[ $CERT_TYPE == "leaf"  ]]; then 

		if [[ $SIGNER_NO == 'r' || $SIGNER_NO == '0'  ]]; then
 			echo -n "SERVER:BOOOOOO> " ; enter2continue
 			cert_index="$CA_DIR/index.txt"
			root_ca_cert=$(find $CA_DIR/ | grep ca.cert.pem)
			cert_chain="$CA_DIR/certs/ca.cert.pem"
			cert_issuer="$CA_DIR/certs/ca.cert.pem"
			
			ocsp_cert=$(find $CA_DIR/certs/ | grep ocsp | grep cert.pem)
			ocsp_priv_key=$(find $CA_DIR/private/ | grep ocsp | grep key.pem)
			
		else

		root_ca_cert=$(find $CA_DIR/ | grep ca.cert.pem)
		cert_chain=$(find $CA_DIR/intermediate$SIGNER_NO/ | grep chain.cert.pem)
		cert_index="$CA_DIR/intermediate$SIGNER_NO/index.txt"
		 
		cert_issuer="$CA_DIR/intermediate$SIGNER_NO/certs/intermediate$SIGNER_NO.cert.pem"
	
		ocsp_cert=$(find $CA_DIR/intermediate$SIGNER_NO/ | grep ocsp | grep cert.pem)
		ocsp_priv_key=$(find $CA_DIR/intermediate$SIGNER_NO | grep ocsp | grep key.pem)
		fi
	fi
	
	##This snippet is only required if one is using an OCSP Responder from a different hierarchy.
	if [[ $OCSP_RESPONDER == "external" ]]; then #echo "Running in Server Mode" 
		echo -n "DEBUG: StandAlone OCSP server> "
		ocsp_cert="$OCSP_CA_DIR/certs/ocsp0.local-2048-sha256-signedbyINT0.cert.pem"
		ocsp_priv_key="$OCSP_CA_DIR/private/ocsp0.local-2048.key.pem"
	fi

	#------------------------------------------------------------------------------------------------
	#CHECK SELECTED PARAMETERS
	#------------------------------------------------------------------------------------------------
	if [[ $DEBUG == '1' ]]; then
	 echo "DEBUG:  Listening Requests on $URL>" ;
	 echo  "DEBUG: ...................................."
 	 echo  "DEBUG:rsigner: ocsp_cert >$ocsp_cert" ; 
 	 echo  "DEBUG:rkey: ocsp_priv_key >$ocsp_priv_key" ; 
	 echo  "DEBUG: ...................................."
 	 echo  "DEBUG:index: cert_index >$cert_index"  ; 
 	 echo -e  "DEBUG:root_ca_cert >$root_ca_cert"  ; 
 	 echo  "DEBUG:CA: cert_chain >$cert_chain"  ; 
 	fi

	#echo $C_DEBUG openssl ocsp -port $URL -sha256 -resp_text -index $cert_index -CA $cert_chain -rkey $ocsp_priv_key -rsigner $ocsp_cert -nrequest 1 
	#------------------------------------------------------------------------------------------------
	#SEND PARAMETERS TO SERVER
	#------------------------------------------------------------------------------------------------
	$C_DEBUG openssl ocsp -port $URL -sha256 -text \
	-index $cert_index \
	-CA $cert_chain \
	-rkey $ocsp_priv_key \
	-rsigner $ocsp_cert \
	-nrequest 1000 \
	-nmin 60 \
	| tee ocsp-$CERT_TYPE$WORKER_NO-$PORT.log

######################################################################################################
#CLIENT
######################################################################################################
 elif [[ $ROLE == "client" ]]; then
 
 	URL="$PC_IP:$PORT"
 	#echo "DEBUG: Sending Request to $URL>" ;
	
 	#if [[ ! $END_CERT_TO_CHECK ]]; then 
 	#	test_cert=$(find $CA_DIR/intermediate$SIGNER_NO | grep signed | grep "test")
 	#	else
 	#		test_cert=$END_CERT_TO_CHECK
 	#fi
	#if [[ ! $test_cert ]]; then echo "No certificate to check. Exit!" ; exit ; fi
	
	#------------------------------------------------------------------------------------------------
	#ASSIGN PARAMETERS
	#------------------------------------------------------------------------------------------------
	#------------------------------------------------------------------------------------------------
	#ASSIGN PARAMETERS| Int[x-1]->Int[x]
	#------------------------------------------------------------------------------------------------
	if [[ $CERT_TYPE == "int" ]]; then 		
		if [[ $SIGNER_NO > "1" ]]; then
			test_cert="$CA_DIR/intermediate$SIGNER_NO/certs/intermediate$SIGNER_NO.cert.pem"
			((SIGNER_NO --))
			cert_chain=$(find $CA_DIR/intermediate$SIGNER_NO/ | grep chain.cert.pem) 	
			cert_issuer="$CA_DIR/intermediate$SIGNER_NO/certs/intermediate$SIGNER_NO.cert.pem"
	 #------------------------------------------------------------------------------------------------
	#ASSIGN PARAMETERS| Root -> Int1
	#------------------------------------------------------------------------------------------------
 		else
 			cert_chain="$CA_DIR/certs/ca.cert.pem"
 			#cert_chain=$(find $CA_DIR/certs/ | grep chain.cert.pem)	
			cert_issuer="$CA_DIR/certs/ca.cert.pem"
			test_cert="$CA_DIR/intermediate$SIGNER_NO/certs/intermediate$SIGNER_NO.cert.pem"
	
 		fi
	fi
	#------------------------------------------------------------------------------------------------
	#ASSIGN PARAMETERS| Int[x]-> Leaf[x]
	#------------------------------------------------------------------------------------------------
	if [[ $CERT_TYPE == "leaf" ]]; then 
		##Special section added if verifying a certificate signed by script4.
		if [[ $SIGNER_NO == "r" || $SIGNER_NO == "0"  ]]; then
 			echo -n "CLIENT:BOOOOOO> " ; enter2continue
			cert_chain="$CA_DIR/certs/ca.cert.pem"
			cert_issuer="$CA_DIR/certs/ca.cert.pem"
			test_cert=$5
			if [[ ! $5 ]]; then echo -ne "Provide a test_cert [Arg 5] certificate to check against>" ; read test_cert ; fi
			break;
		else
			cert_chain=$(find $CA_DIR/intermediate$SIGNER_NO/ | grep chain.cert.pem) 
			cert_issuer="$CA_DIR/intermediate$SIGNER_NO/certs/intermediate$SIGNER_NO.cert.pem"
			test_cert=$(find $CA_DIR/intermediate$SIGNER_NO | grep signed | grep "test")
		fi
	fi
	
 	#if [[ ! $PORT ]]; then echo -n "Enter PORT >" ; read PORT ; else PORT=$4 ; fi
 	#URL="$PC_IP:$PORT"
 	##echo -n "DEBUG: $URL>" ; enter2continue
	
 	if [[ $DEBUG == '1' ]]; then
 	#------------------------------------------------------------------------------------------------
	#CHECK WHAT PARAMETERS?
	#------------------------------------------------------------------------------------------------
	 echo  "DEBUG: ...................................."
 	 echo  "DEBUG:CA: cert_chain >$cert_chain"  ; 
 	 echo  "DEBUG: ...................................."
 	 echo  "DEBUG:cert_issuer >$cert_issuer"  ; 
 	 echo  "DEBUG:test_cert >$test_cert"  ; 
 	 echo -n  "...................................." ; enter2continue
	fi
	
	#------------------------------------------------------------------------------------------------
	#SEND SELECTED PARAMETERS AS CLIENT REQUEST
	#------------------------------------------------------------------------------------------------
  	$C_DEBUG openssl ocsp -CAfile $cert_chain \
  	-url http://$URL -text \
  	-issuer $cert_issuer \
  	-cert $test_cert | grep --line-buffered --color "good\|false\|unknown\|successful\|$" 
	
	echo "**************************************************************************************************************"
	enter2continue ; clear
######################################################################################################
#JUST LOOKING AT THE LOF FILE WITH EASE
######################################################################################################
elif [[ $ROLE == "answer" ]]; then
	tail -f --follow=name -n 500 ocsp-$CERT_TYPE$SIGNER_NO-$PORT.log \
	| grep --line-buffered --color "OCSP Response Data" -A 25 \
	| grep --line-buffered --color "Cert Status\|good\|false\|unknown\|successful\|$" 
fi


	##./3.launchOCSP.sh server
	#| grep --line-buffered --color "OCSP Response Data" -A 25 \
	#| grep --line-buffered --color "Cert Status\|good\|false\|unknown\|successful\|$" 
