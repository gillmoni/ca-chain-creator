#!/bin/bash
source ../cnf/ca_settings.txt

#openssl verify -CAfile $CA_DIR/intermediate1/certs/ca-cert-int1.chain.cert.pem $CA_DIR/intermediate1/certs/test1.example.local-2048-sha256-signedbyINT1.cert.pem ; enter2continue
#openssl verify -CAfile $CA_DIR/intermediate2/certs/ca-cert-int2.chain.cert.pem $CA_DIR/intermediate2/certs/test2.example.local-2048-sha256-signedbyINT2.cert.pem ; enter2continue
if [[ ! $1 ]]; then exit ; fi

for (( i = 1; i <= $1; i++ )); do
 echo -n  "DEBUG: Testing for Leaf$i> k"
 openssl verify -CAfile $CA_DIR/intermediate$i/certs/ca-cert-int$i.chain.cert.pem $CA_DIR/intermediate$i/certs/test$i.example.local*signedbyINT$i.cert.pem ;
done

