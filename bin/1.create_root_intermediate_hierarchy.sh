#!/bin/bash
source ../cnf/ca_settings.txt
################################################################################################################################
function root_create_directories(){
 #echo -n "DEBUG: Creating Root Directory." ; enter2continue
 if [[ ! -f $BASE_ROOT_CONF ]]; then  echo "Root config file doesn't exist. Exiting!!"; exit ; fi 
 mkdir $CA_DIR ; 
 cp $BASE_ROOT_CONF "$CA_DIR/openssl.cnf"

 
cd $CA_DIR ;
 sed -i -- "s#/root/ca#$CA_DIR#g" $CA_DIR/openssl.cnf
 
 #Replace "192.168.0.5" in openssl.cnf by PC_IP
 sed -i -- "s#192.168.0.5#$PC_IP#g" $CA_DIR/openssl.cnf

 mkdir certs crl newcerts private $BUILD csr
 chmod 700 private ; touch index.txt ; 
 #echo 9000 > serial
 openssl rand -hex 2 > serial
 #echo "" $BUILD/root.$CRT_FILE
}
################################################################################################################################
function root_create_key_old(){
  #echo -n "DEBUG: Creating Root Key." ; enter2continue
  cd $CA_DIR
  R_KEY="$CA_DIR/private/ca.key.pem"
  if [[ PROTECT_KEY == 1 ]]; then $C_DEBUG openssl genrsa -aes256 -passout pass:admin -out $R_KEY $KSIZE ;
  else $C_DEBUG openssl genrsa -out $R_KEY $KSIZE ; 
  fi
  chmod 400 $R_KEY
}
################################################################################################################################
function root_create_key(){
  #echo -n "DEBUG: Creating Root Key" ; enter2continue
  R_KEY="$CA_DIR/private/ca.key.pem"
  cd $CA_DIR
  if [[ "$TYPE" = "ec" ]]; then root_create_key_ec $R_KEY
   else root_create_key_rsa $R_KEY ;  fi
  chmod 400 $R_KEY 
 # echo -n "FILE: Saved $R_KEY, $(stat -c%s $R_KEY) bytes" ; enter2continue
}
################################################################################################################################
function root_create_key_ec(){
 #echo -n "DEBUG: Generating a ROOT EC private key> " ; enter2continue
 KSIZE="521" ;  SSL_CURVE=secp"$KSIZE"r1 ;
  
 #$C_DEBUG openssl ecparam  -out $R_KEY -name $SSL_CURVE -genkey
 $C_DEBUG openssl ecparam  -out $R_KEY -noout -name $SSL_CURVE -genkey

  if [[ PROTECT_KEY == 1 ]]; then $C_DEBUG openssl ecparam -passout pass:admin -out $R_KEY -noout -name $SSL_CURVE -genkey
   else $C_DEBUG openssl ecparam  -out $R_KEY -noout -name $SSL_CURVE -genkey
  fi
}
################################################################################################################################
function root_create_key_rsa(){
  #echo -n "DEBUG: Generating a ROOT RSA private key> " ; enter2continue
 if [[ PROTECT_KEY == 1 ]]; then $C_DEBUG openssl genrsa -aes256 -passout pass:admin -out $R_KEY $KSIZE ; 
  else $C_DEBUG openssl genrsa -out $R_KEY $KSIZE
 fi
 #echo -n "DEBUG:Generating a RSA key> " ; enter2continue
}
################################################################################################################################
################################################################################################################################
function root_create_cert(){
  #echo -n "DEBUG: Creating Root Cert." ; enter2continue
    $C_DEBUG openssl req -config openssl.cnf \
        -subj '/C=CA/ST=ON/O=MCorp-Canada/OU=MCorp-CA-Authority/CN=MCorpRootCA/emailAddress=EMAIL/' \
        -passin pass:admin \
        -key private/ca.key.pem \
        -new -x509 -days 7300 -sha256 -extensions v3_ca \
        -out certs/ca.cert.pem
}
################################################################################################################################
################################################################################################################################
function int_create_directories(){
  #echo -n "DEBUG: Creating Intermediate$INT_NUM Directory" ; enter2continue
  if [[ ! -f $BASE_INT_CONF ]]; then  echo "Intermediate config file doesn't exist. Exiting!!"; exit ; fi 
  mkdir $CA_DIR/intermediate$INT_NUM
  
  cp $BASE_INT_CONF "$CA_DIR/intermediate$INT_NUM/openssl.cnf"
  
  #Replace "192.168.0.5" in openssl.cnf by PC_IP
  sed -i -- "s#192.168.0.5#$PC_IP#g" $CA_DIR/intermediate$INT_NUM/openssl.cnf

  #ln -fs $CA_DIR/intermediate $CA_DIR/intermediate0
  if [[ ! $INT_NUM ]]; then  custom_CRT=1000 ; else  custom_CRT="c$INT_NUM""000" ; fi
  #echo -n "DEBUG: INT_NUM value is> $INT_NUM, custom_CRT value is> $custom_CRT" ; enter2continue
 
  FILE="$CA_DIR/intermediate$INT_NUM/index.txt" #Check whether intermediate already exists. 
  if [[ -f $FILE ]]; then  echo "intermediate$INT_NUM already exits. Exiting!!"; exit ; fi 

  #Make changes to each intermediate num.
  sed -i -- "s/\/intermediate/\/intermediate$INT_NUM/g" $CA_DIR/intermediate$INT_NUM/openssl.cnf
  sed -i -- "s#/root/ca#$CA_DIR#g" $CA_DIR/intermediate$INT_NUM/openssl.cnf

  #Change OCSP info for next certificate for [intermediate_ca] block in root/openssl.cnf"
  NEW_INT_NUM=$(($INT_NUM+1))
  #echo -n "DEBUG SED:Changing from from $PC_IP:256$INT_NUM > $PC_IP:256$NEW_INT_NUM" ; enter2continue
 # sed -i -- "s#$PC_IP:256$INT_NUM#$PC_IP:256$NEW_INT_NUM#g" $CA_DIR/intermediate$INT_NUM/openssl.cnf #For intermediates
 # sed -i -- "s#$PC_IP:2560#$PC_IP:256$NEW_INT_NUM#g" $CA_DIR/intermediate$INT_NUM/openssl.cnf #For Base

  sed -i -- "s#$PC_IP:256$INT_NUM#$PC_IP:256$NEW_INT_NUM#g" $CA_DIR/intermediate$INT_NUM/openssl.cnf #For intermediates
  sed -i -- "s#$PC_IP:2560#$PC_IP:256$NEW_INT_NUM#g" $CA_DIR/intermediate$INT_NUM/openssl.cnf #For Base

  sed -i -- "s#$PC_IP:3560#$PC_IP:356$INT_NUM#g" $CA_DIR/intermediate$INT_NUM/openssl.cnf #For intermediates
  #sed -i -- "s#$PC_IP:3560#$PC_IP:356$NEW_INT_NUM#g" $CA_DIR/intermediate$INT_NUM/openssl.cnf #For Base
  
  cd $CA_DIR/intermediate$INT_NUM
  mkdir certs crl csr newcerts private $BUILD
  chmod 700 private ; touch index.txt
  
  #echo $custom_CRT > serial
  openssl rand -hex $INT_NUM > serial
  
  #Add a crlnumber file to the intermediate CA directory tree. crlnumber is used to keep track of certificate revocation lists.
  echo $custom_CRT > $CA_DIR/intermediate$INT_NUM/crlnumber
  #openssl rand -hex $INT_NUM > $CA_DIR/intermediate$INT_NUM/crlnumber
}
################################################################################################################################
function int_create_key(){
  #echo -n "DEBUG: Creating Intermediate$INT_NUM Key" ; enter2continue
  F_KEY="$CA_DIR/intermediate$INT_NUM/private/intermediate$INT_NUM.key.pem"
  cd $CA_DIR
  if [[ "$TYPE" = "ec" ]]; then int_create_key_ec $F_KEY
   else int_create_key_rsa $F_KEY ;  fi
  chmod 400 $F_KEY 
  #echo -n "FILE: Saved $F_KEY, $(stat -c%s $F_KEY) bytes" ; enter2continue
}
################################################################################################################################
function int_create_key_ec(){
 #echo -n "DEBUG: Generating a Intermediate EC private key> " ; enter2continue
 KSIZE="521" ;  SSL_CURVE=secp"$KSIZE"r1 ;
  
 #$C_DEBUG openssl ecparam  -out $F_KEY -name $SSL_CURVE -genkey
 $C_DEBUG openssl ecparam  -out $F_KEY -noout -name $SSL_CURVE -genkey

  if [[ PROTECT_KEY == 1 ]]; then $C_DEBUG openssl ecparam -passout pass:admin -out $F_KEY -noout -name $SSL_CURVE -genkey
   else $C_DEBUG openssl ecparam  -out $F_KEY -noout -name $SSL_CURVE -genkey
  fi
}
################################################################################################################################
function int_create_key_rsa(){
  #echo -n "DEBUG: Generating a Intermediate RSA private key> " ; enter2continue
 if [[ PROTECT_KEY == 1 ]]; then $C_DEBUG openssl genrsa -aes256 -passout pass:admin -out $F_KEY $KSIZE ; 
  else $C_DEBUG openssl genrsa -out $F_KEY $KSIZE
 fi
 #echo -n "DEBUG:Generating a RSA key> " ; enter2continue
 $C_DEBUG openssl genrsa -out $F_KEY $KSIZE
}
################################################################################################################################
function int_create_csr(){
  #echo -n "DEBUG: Creating Intermediate$INT_NUM CSR" ; enter2continue
  cd $CA_DIR
  $C_DEBUG openssl req -config intermediate$INT_NUM/openssl.cnf -new -sha256 \
    -subj '/C=CA/ST=ON/O=MCorp-Canada/OU=MCorp-CA-Authority'$INT_NUM'/CN=MCPInt'$INT_NUM'/emailAddress=EMAIL/' \
    -passin pass:admin \
    -key $F_KEY \
    -out intermediate$INT_NUM/csr/intermediate$INT_NUM.csr.pem
}
################################################################################################################################
function int_sign_by_root(){
 #echo -n "DEBUG: Signing Intermediate$INT_NUM by Root" ; enter2continue
 cd $CA_DIR
 C_CONFIG="$CA_DIR/openssl.cnf"
 #$C_DEBUG openssl ca -config $C_CONFIG -extensions v3_intermediate_ca$INT_NUM \
 $C_DEBUG openssl ca -config $C_CONFIG -extensions v3_intermediate_ca \
  -days 3650 -notext -md sha256 \
  -passin pass:admin -batch \
  -in intermediate$INT_NUM/csr/intermediate$INT_NUM.csr.pem \
  -out intermediate$INT_NUM/certs/intermediate$INT_NUM.cert.pem

 cat intermediate$INT_NUM/certs/intermediate$INT_NUM.cert.pem >> intermediate$INT_NUM/$BUILD/intermediate$INT_NUM.cert.pem.$CRT_FILE

 chmod 444 intermediate$INT_NUM/certs/intermediate$INT_NUM.cert.pem

 #Change OCSP info for next certificate for [intermediate_ca] block in root/openssl.cnf"
 sed -i -- "s#$PC_IP:256$INT_NUM#$PC_IP:256$NEW_INT_NUM#g" $CA_DIR/openssl.cnf

}
################################################################################################################################
function int_sign_by_other_int(){
#echo -n "DEBUG: Signing Intermediate$INT_NUM by Intermediate$SIGN_BY" ; read input </dev/tty
 cd $CA_DIR
 C_CONFIG="$CA_DIR/intermediate$SIGN_BY/openssl.cnf"
 
 #echo "DEBUG: Using config from $C_CONFIG" ; enter2continue
 #$C_DEBUG openssl ca -config $C_CONFIG -extensions v3_intermediate_ca$INT_NUM \
 $C_DEBUG openssl ca -config $C_CONFIG -extensions v3_intermediate_ca \
  -days 3650 -notext -md sha256 \
  -passin pass:admin -batch \
  -in intermediate$INT_NUM/csr/intermediate$INT_NUM.csr.pem \
  -out intermediate$INT_NUM/certs/intermediate$INT_NUM.cert.pem

  #echo "$INT_NUM,active,intermediate$INT_NUM" > intermediate$INT_NUM/$BUILD/intermediate$INT_NUM.cert.pem.$CRT_FILE
  cat intermediate$INT_NUM/certs/intermediate$INT_NUM.cert.pem >> intermediate$INT_NUM/$BUILD/intermediate$INT_NUM.cert.pem.$CRT_FILE

  chmod 444 intermediate$INT_NUM/certs/intermediate$INT_NUM.cert.pem

  echo "Intermediate$INT_NUM signed by Intermediate$SIGN_BY: Symbolic link for intermediate$INT_NUM to intermediate$SIGN_BY/certs/, " ;
  ln -s intermediate$INT_NUM/certs/intermediate$INT_NUM.cert.pem intermediate$SIGN_BY/certs/intermediate$INT_NUM.cert.pem       
  chmod 444 intermediate$SIGN_BY/certs/intermediate$SIGN_BY.cert.pem

  #echo -n "DEBUG: Done Signing Intermediate$INT_NUM by Intermediate$SIGN_BY" ; read input </dev/tty
}
################################################################################################################################
function revoke_int_cert(){ 
 #echo -n "DEBUG: I'm in INT recovation mode."; enter2continue
 INT_SIGN=$2
 #echo -n "DEBUG: INT_SIGN > $INT_SIGN" ; enter2continue

 if [[ $INT_SIGN == "0" || $INT_SIGN == "r" ]] ; then rev_file="$CA_DIR/index.txt" ;
   CERT_DIR="$CA_DIR"
  else 
   rev_file="$CA_DIR/intermediate$INT_SIGN/index.txt" ;
    CERT_DIR="$CA_DIR/intermediate$INT_SIGN"
 fi

 while read rev_line; do
  B=($rev_line) ; BNAME=$(echo ${B[4]}  | grep -o -P '(?<=CN=).*(?=/email)')
  FNAME="MCPInt$1"
  #echo "DEBUG: FNAME = $FNAME, BNAME = $BNAME"    
  
  REV_STATUS="${B[0]}" ;
  #echo "DEBUG: REV_STATUS = $REV_STATUS"
  
  if [[ "$BNAME" != "$FNAME" ]]; then continue ; fi
  #if [[ "$BNAME" == "$FNAME" ]]; then echo -n "Revoke match!!> " ; fi    
  
  if [[ $REV_STATUS == "R" ]]; then REV_ID="${B[2]}" ; echo "$REV_ID-> Already Revoked. Skipping!!" ; fi
  if [[ $REV_STATUS == "V" ]]; then REV_ID="${B[2]}" ;
   #echo "DEBUG: Revoke File is <$rev_file>"
   echo "DEBUG: Certificate to be revoked is> $REV_ID";
   echo "DEBUG: Certificate parameters> ${B[4]}"
   
   
   $C_DEBUG openssl ca -config $CERT_DIR/openssl.cnf \
    -revoke $CERT_DIR/newcerts/$REV_ID.pem \
    -batch -passin pass:admin
  fi      
  done < $rev_file 
}
################################################################################################################################
function root_add_to_cert_chain(){
 #echo -n "DEBUG:Adding ROOTCA to root.$CRT_FILE. File <>" ; enter2continue
 R_FILE="$CA_DIR/certs/ca.cert.pem"
 cat $R_FILE >> $CA_DIR/$BUILD/root.$CRT_FILE
 echo -e "\n" >> $CA_DIR/$BUILD/root.$CRT_FILE

 cat $R_FILE >> $CA_DIR/$BUILD/$CRT_FILE

 ######################################################################################################
 #If OCSP_Responder is external then prepate /$BUILD/$CRT_FILE properly.
 ######################################################################################################
 if [[ $OCSP_RESPONDER == 'external' ]]; then
  echo -n "DEBUG: OCSP_RESPONDER is $OCSP_RESPONDER." ; 
  check_cert=$(find $OCSP_CA_DIR/certs/ | grep ocsp0) ;
  #echo -n "DEBUG: Found certificate $check_cert." ; enter2continue
  if [[ -f $check_cert ]]; then
    #echo -n "DEBUG: Yes StandAlone OCSP_RESPONDER exists." ; enter2continue
    cat $check_cert >> $CA_DIR/$BUILD/$CRT_FILE
  fi
 fi

 #cat $(find $CA_DIR/ | grep "ca.cert.pem")>> $CA_DIR/certs/ca-chain.cert.pem
}
################################################################################################################################
function int_add_to_cert_chain(){
 #cd $CA_DIR
 I_FILE="$CA_DIR/intermediate$INT_NUM/certs/intermediate$INT_NUM.cert.pem"
 if [[ -f $I_FILE ]]; then
 #echo -n "DEBUG:Adding intermediate$INT_NUM to root.$CRT_FILE" ; enter2continue
 echo "$INT_NUM,active,signingca" >> $CA_DIR/$BUILD/root.$CRT_FILE
 cat $I_FILE >> $CA_DIR/$BUILD/root.$CRT_FILE
 echo -e "\n" >> $CA_DIR/$BUILD/root.$CRT_FILE
 
 cat $I_FILE >> $CA_DIR/certs/ca-chain.cert.pem 
else
  echo -n "Intermediate$INT_NUM file didn't exist. Skipping." ; enter2continue
 fi

}
################################################################################################################################
################################################################################################################################
MODE="$1" ; INT_NUM="$3" ; SIGN_BY="$4" ; TYPE="$2"
if [[ ! $MODE ]]; then  echo "Arg 1, Mode of operation> :~# script [root/int/rev/chain]>" ; exit ; fi
################################################################################################################################
if [[ "$MODE" == "root" ]]; then  
 FILE="$CA_DIR/index.txt"
 if [[ -f $FILE ]]; then  echo "Root already exits. Exiting!!"; exit; fi #Check whether ROOT_CA already exists. 
 root_create_directories ; root_create_key ; root_create_cert ;
 root_add_to_cert_chain
 
 elif [[ "$MODE" == "int" ]]; then INT_NUM="$3" ;
   int_create_directories $INT_NUM ; int_create_key $INT_NUM ; int_create_csr $INT_NUM 

  if [[ $SIGN_BY == "R" || $SIGN_BY == "r" || $SIGN_BY == "0" || ! $SIGN_BY ]]; then int_sign_by_root
   else int_sign_by_other_int $INT_NUM $SIGN_BY
  fi
    int_add_to_cert_chain
fi
################################################################################################################################

if [[ "$MODE" == "rev" ]]; then
  revoke_int_cert $2 $3
  #echo "DEBUG: Calling revoke_int_cert,  Passing $2";
fi

if [[ "$MODE" == "chain" ]]; then
 
cat $(find $CA_DIR/ | grep "ca.cert.pem")>> $CA_DIR/certs/ca-chain.cert.pem
chmod 444 $CA_DIR/certs/ca-chain.cert.pem

#We don't need it so removing. #Uncomment if required.
#rm $CA_DIR/$BUILD/root.$CRT_FILE
 echo "Done! Certificate ready /root/ca/certs/ca-chain.cert.pem, Send this to browser!!"
fi
