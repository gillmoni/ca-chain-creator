#!/bin/bash
source ../cnf/ca_settings.txt
#echo -n "Going Into ROOT/CA> " ; enter2continue
if [[ ! $1 ]]; then echo "> script [pattern] [count]" ; exit ; fi
if [[ ! $2 ]]; then COUNT=3 ; else COUNT=$2 ; fi

cd $CA_DIR/ ; 

echo -n "$1 for $CRT_FILE> " ; enter2continue
 vc $CA_DIR/$BUILD/$CRT_FILE $CRT_FILE | grep -i "$1"



for (( i = 1; i <= $COUNT; i++ )); do
 echo -n "$1 for Leaf $i> " ; enter2continue
  vc $CA_DIR/intermediate$i/$BUILD/test$i.example.local-2048.$BUILD.$F_EXT | grep -i "$1"
 
 echo -n "$1 for Intermediate $i> " ; enter2continue
  vc $CA_DIR/intermediate$i/certs/intermediate$i.cert.pem | grep -i "$1"

done