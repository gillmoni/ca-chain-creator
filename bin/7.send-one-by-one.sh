#!/bin/bash
source ../cnf/ca_settings.txt

function enter2continue(){ 
  read input </dev/tty 
}

if [[ ! $1 ]]; then
	echo -n "Give me an IP host> " ; read HOST
 else
	HOST="$1"
fi

cd $CA_DIR ; 
echo "+++ $(date) +++########################" >> /var/log/rcom.log

#Skip the line and send or don't send a specific Leaf
if [[ $2 == '0' || $2 == 's' ]]; then
 #echo -n "Going Into $CA_DIR> " ; enter2continue
 echo -n "Press ENTER to send $CRT_FILE> " ; enter2continue
 $SEND_TOOL p $HOST $CA_DIR/$BUILD/$CRT_FILE $CRT_FILE 
 echo "+Sending $CA_DIR/$BUILD/$CRT_FILE $CRT_FILE to $1" >> /var/log/rcom.log
 echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" >> /var/log/rcom.log
#else
 #echo "Invalid Parameter. ./script [host] [s/invalid] [leaf_no]."
fi

#Skip the line and send a specific Leaf
if [[ $3 || $2 == '1' || $2 == '2' || $2 == '3'  || $2 == '4'  || $2 == '5'  || $2 == '6'  || $2 == '7' ]]; then
 echo -n "Press ENTER to send Leaf $2> " ; enter2continue
 $SEND_TOOL p $HOST $CA_DIR/intermediate$2/$BUILD/test$2.example.*.$BUILD.$F_EXT $F_EXT 
 echo "+Sending $CA_DIR/intermediate$2/$BUILD/test$2.example.*.$BUILD.$F_EXT $F_EXT to $HOST" >> /var/log/rcom.log
 echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" >> /var/log/rcom.log
fi

#Default SET send everything one by one.
if [[ ! $2 ]]; then
 #echo -n "Going Into $CA_DIR> " ; enter2continue
 echo -n "Press ENTER to send $CRT_FILE> " ; enter2continue
 $SEND_TOOL p $HOST $CA_DIR/$BUILD/$CRT_FILE $CRT_FILE 
 echo "+Sending $CA_DIR/$BUILD/$CRT_FILE $CRT_FILE to $HOST" >> /var/log/rcom.log
 echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" >> /var/log/rcom.log

 echo -n "Press ENTER to send Leaf 1> " ; enter2continue
 $SEND_TOOL p $HOST $CA_DIR/intermediate1/$BUILD/test1.example.*.$BUILD.$F_EXT $F_EXT 
 #echo "+Sending $CA_DIR/intermediate1/$BUILD/test1.example.*.$BUILD.$F_EXT $F_EXT to $1" >> /var/log/rcom.log
 echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" >> /var/log/rcom.log
 
 echo -n "Press ENTER to send Leaf 2> " ; enter2continue
 $SEND_TOOL p $HOST $CA_DIR/intermediate2/$BUILD/test2.example.*.$BUILD.$F_EXT $F_EXT 
 #echo "+Sending $CA_DIR/intermediate2/$BUILD/test2.example.*.$BUILD.$F_EXT $F_EXT to $1" >> /var/log/rcom.log
 echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" >> /var/log/rcom.log
 
 echo -n "Press ENTER to send Leaf 3> " ; enter2continue
 $SEND_TOOL p $HOST $CA_DIR/intermediate3/$BUILD/test3.example.*.$BUILD.$F_EXT $F_EXT 
 #echo "+Sending $CA_DIR/intermediate3/$BUILD/test3.example.*.$BUILD.$F_EXT $F_EXT to $1" >> /var/log/rcom.log
 echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" >> /var/log/rcom.log

echo "##########################################################" >> /var/log/rcom.log
fi
