#!/bin/bash
source ../cnf/ca_settings.txt
R_CERT=$(find $CA_DIR/certs/ca.cert.pem)
BASE_DIR=$CA_DIR
#################################################################################################################################################################
function correct_args(){
  echo "Arg 2, Which intermediate CA server would sign it, enter number> " ;
  echo "Arg 3, Hashing algo for certificate (e.g  For RSA md5, sha128, sha256, sha384, sha512)>" ; 
  echo "Arg 4, Key size for certificate (e.g For RSA 512, 1024, 2048 ... 3072, 4096, For EC- 384, 521)>" ; 
  echo "Arg 6, Type of certificate (e.g rsa only)> "; exit
}
#################################################################################################################################################################
#################################################################################################################################################################
function leaf_create_key_old(){
#echo -n "DEBUG:Creating as Key>" ; enter2continue
cd $CA_DIR
if [[ PROTECT_KEY == 1 ]]; then $C_DEBUG openssl genrsa -aes256 -passout pass:admin -out intermediate$INT_SIGN/private/$FNAME-$KSIZE.key.pem $KSIZE ;
 else $C_DEBUG openssl genrsa -out intermediate$INT_SIGN/private/$FNAME-$KSIZE.key.pem $KSIZE ; fi
chmod 400 intermediate$INT_SIGN/private/$FNAME-$KSIZE.key.pem
}
################################################################################################################################
function leaf_create_key(){
  #echo -n "DEBUG: Creating Leaf Key" ; enter2continue
  L_KEY="$CA_DIR/private/$FNAME-$KSIZE.key.pem"
  cd $CA_DIR
  if [[ "$TYPE" = "ec" ]]; then leaf_create_key_ec $L_KEY
   else leaf_create_key_rsa $L_KEY ;  fi
  chmod 400 $L_KEY 
  #echo -n "FILE: Saved $L_KEY, $(stat -c%s $L_KEY) bytes" ; enter2continue
}
################################################################################################################################
function leaf_create_key_ec(){
 #echo -n "DEBUG: Generating a Leaf EC private key> " ; enter2continue
 
 if [[ "$KSIZE" = "384" || "$KSIZE" =  "521" ]]; then SSL_CURVE=secp"$KSIZE"r1 ;
 elif [[ "$KSIZE" = "256" || "$KSIZE" =  "521" ]]; then SSL_CURVE=prime"256"v1 ; 
 else 
  echo "Invalid EC key size. Exit!" ; exit
  fi 
  
 #$C_DEBUG openssl ecparam  -out $L_KEY -name $SSL_CURVE -genkey
 $C_DEBUG openssl ecparam  -out $L_KEY -noout -name $SSL_CURVE -genkey

  if [[ PROTECT_KEY == 1 ]]; then $C_DEBUG openssl ecparam -passout pass:admin -out $L_KEY -noout -name $SSL_CURVE -genkey
   else $C_DEBUG openssl ecparam  -out $L_KEY -noout -name $SSL_CURVE -genkey
  fi
}
################################################################################################################################
function leaf_create_key_rsa(){
  #echo -n "DEBUG: Generating a Leaf RSA private key> " ; enter2continue
 if [[ PROTECT_KEY == 1 ]]; then $C_DEBUG openssl genrsa -aes256 -passout pass:admin -out $L_KEY $KSIZE ; 
  else $C_DEBUG openssl genrsa -out $L_KEY $KSIZE
 fi
 #echo -n "DEBUG:Generating a RSA key> " ; enter2continue
 $C_DEBUG openssl genrsa -out $L_KEY $KSIZE
}
################################################################################################################################
#################################################################################################################################################################
function create_csr(){
 #echo -n "DEBUG:Creating CSR>" ; enter2continue
 cd $CA_DIR
 
 C_CONFIG="$CA_DIR/openssl.cnf"
 F_KEY="$CA_DIR/private/$FNAME-$KSIZE.key.pem"
 F_CSR="$CA_DIR/csr/$FNAME-$KSIZE-$CTYPE.csr.pem"
 
 #echo -n DEBUG: C_CONFIG=$CA_DIR/openssl.cnf ; enter2continue
 #echo -n DEBUG: F_KEY=$CA_DIR/private/$FNAME-$KSIZE.key.pem ; enter2continue
 #echo -n DEBUG: F_CSR=$CA_DIR/csr/$FNAME-$KSIZE-$CTYPE.csr.pem; enter2continue
 
 $C_DEBUG openssl req -config $C_CONFIG \
  -subj "$SUBJ" \
  -passin pass:admin \
  -key $F_KEY \
  -new -$CTYPE -out $F_CSR
 #echo -n "DEBUG:Done Creating CSR>" ; enter2continue
 #echo -n "FILE: Saved $F_CSR, $(stat -c%s $F_CSR) bytes" ; enter2continue
}
#################################################################################################################################################################
function sign_as_ocsp() {
 #echo -n "DEBUG:Signing as OCSP>" ; enter2continue
 cd $CA_DIR
 F_KEY="$CA_DIR/private/intermediate$INT_SIGN.key.pem"
 F_CERT="$CA_DIR/certs/intermediate$INT_SIGN.cert.pem"
 
 if [[ $INT_SIGN == '0' || $INT_SIGN == 'r' ]]; then #Sign OCSP by Root.
  INT_SIGN='0';
  #echo -n "DEBUG: Started signing OCSP by RootCA." ; enter2continue
  F_KEY="$CA_DIR/private/ca.key.pem"
  F_CERT="$CA_DIR/certs/ca.cert.pem"
 fi

 L_CERT="$CA_DIR/certs/$FNAME-$KSIZE-$CTYPE-signedbyINT$INT_SIGN.cert.pem"
 RL_CERT="$CA_DIR/$BUILD/$FNAME-$KSIZE.$BUILD.$F_EXT"

 $C_DEBUG openssl ca -config openssl.cnf \
  -extensions ocsp -days $DAYS -notext -md $CTYPE \
  -keyfile $F_KEY \
  -cert $F_CERT \
  -passin pass:admin -batch \
  -in $F_CSR \
  -out $L_CERT

 chmod 444 $L_CERT
 #echo -n "FILE: Saved $L_CERT, $(stat -c%s $L_CERT) bytes" ; enter2continue
 #echo -n "DEBUG:Done Signing as OCSP>" ; enter2continue
}
#################################################################################################################################################################
function sign_as_serv_cert(){
 #echo -n "DEBUG:Signing as server_cert>" ; enter2continue
 cd $CA_DIR
 F_KEY="$CA_DIR/private/intermediate$INT_SIGN.key.pem"
 F_CERT="$CA_DIR/certs/intermediate$INT_SIGN.cert.pem"
 
 C_CONFIG="$CA_DIR/openssl.cnf"
 L_CERT="$CA_DIR/certs/$FNAME-$KSIZE-$CTYPE-signedbyINT$INT_SIGN.cert.pem"
 RL_CERT="$CA_DIR/$BUILD/$FNAME-$KSIZE.$BUILD.$F_EXT"

 if [[ $INT_SIGN == '0' || $INT_SIGN == 'r' ]]; then #Sign server_cert by Root.
 INT_SIGN=0 ;
 #echo -n "DEBUG: Started signing server_cert by RootCA." ; enter2continue
  F_KEY="$CA_DIR/private/ca.key.pem"
  F_CERT="$CA_DIR/certs/ca.cert.pem"
  L_CERT="$CA_DIR/certs/$FNAME-$KSIZE-$CTYPE-signedbyRootCA.cert.pem"
 fi
 
 $C_DEBUG openssl ca -config $C_CONFIG \
  -extensions server_cert -days $DAYS -notext -md $CTYPE \
  -keyfile $F_KEY \
  -cert $F_CERT \
  -passin pass:admin -batch \
  -in $F_CSR \
  -out $L_CERT
     
 chmod 444 $L_CERT

 #Sending stuff for $BUILD format
 cat $L_CERT > $CA_DIR/$BUILD/$FNAME-$KSIZE.$BUILD.$F_EXT
 #echo -n "DEBUG: $?" ; enter2continue
 echo "" > $CA_DIR/certs/ca-cert-int$INT_SIGN.chain.cert.pem
 for (( i = $INT_SIGN; i > 0; i-- )); do
     #This is special occurance: DONT'T change BASE_DIR with CA_DIR.
     cat $BASE_DIR/intermediate$i/certs/intermediate$i.cert.pem >> $BASE_DIR/intermediate$INT_SIGN/$BUILD/$FNAME-$KSIZE.$BUILD.$F_EXT
     #echo -n "DEBUG: Adding cat $BASE_DIR/intermediate$i/certs/intermediate$i.cert.pem >> $BASE_DIR/intermediate$INT_SIGN/$BUILD/$FNAME-$KSIZE.$BUILD.$F_EXT" ; enter2continue
     #echo -n "DEBUG: $?" ; enter2continue
     cat $BASE_DIR/intermediate$i/certs/intermediate$i.cert.pem >> $BASE_DIR/intermediate$INT_SIGN/certs/ca-cert-int$INT_SIGN.chain.cert.pem
     #echo -n "DEBUG: Adding cat $BASE_DIR/intermediate$i/certs/intermediate$i.cert.pem >> $BASE_DIR/intermediate$INT_SIGN/certs/ca-cert-int$INT_SIGN.chain.cert.pem" ; enter2continue
     #echo -n "DEBUG: $?" ; enter2continue  
 done
 
 cat $CA_DIR/private/$FNAME-$KSIZE.key.pem >> $CA_DIR/$BUILD/$FNAME-$KSIZE.$BUILD.$F_EXT
 #echo -n "DEBUG: $?" ; enter2continue
 
  #echo -n "FILE: Saved $L_CERT, $(stat -c%s $L_CERT) bytes" ; enter2continue
  #echo -n "FILE: Saved $RL_CERT, $(stat -c%s $RL_CERT) bytes" ; enter2continue
 #echo -n "DEBUG: Done Signing as server_cert>" ; enter2continue
} 
#################################################################################################################################################################
function revoke_cert(){ 
#echo "DEBUG: I'm in recovation mode."; enter2continue
if [[ $INT_SIGN == "0" ]]; then  INT_SIGN=""; fi
rev_file="$CA_DIR/index.txt"
#echo "DEBUG: rev_file is $rev_line"

 while read rev_line; do
  B=($rev_line) ; BNAME=$(echo ${B[4]}  | grep -o -P '(?<=CN=).*(?=/email)')
  #echo "DEBUG: FNAME = $FNAME, BNAME = $BNAME"    
  ##DO NOT COMMENT THIS LINE BELOW. IT SKIPS IF COMMON-NAME OF CERTIFICATES DON'T MATCH.
  if [[ "$BNAME" != "$FNAME" ]]; then continue ; fi
  #if [[ "$BNAME" == "$FNAME" ]]; then echo -n "Revoke match!!> " ; fi    
  REV_STATUS="${B[0]}" ;
  if [[ $REV_STATUS == "R" ]]; then REV_ID="${B[3]}" ; echo "$REV_ID-> Already Revoked. Skipping!!" ; fi
  if [[ $REV_STATUS == "V" ]]; then REV_ID="${B[2]}" ;
   echo "DEBUG: Revoke File is <$rev_file>"
   echo "DEBUG: Certificate to be revoked is $REV_ID";
   
   $C_DEBUG openssl ca -config $CA_DIR/openssl.cnf \
    -revoke $CA_DIR/newcerts/$REV_ID.pem \
    -batch -passin pass:admin
  fi      
  done < $rev_file 
  #echo "DEBUG: I'm in exiting recovation mode."; enter2continue
}
#################################################################################################################################################################
function skip_cert(){ 
 echo -n ""
 #echo "Skipping for '$a'"
}
#################################################################################################################################################################
#################################################################################################################################################################

#Main of the program
if [[ ! $1 ]]; then echo "No file specified, specify a file to read certificate descriptions from. Exiting!!" ; exit ; fi
while read line ; do a=($line) #Read line by line from configuration file.  
 SUBJ="${a[0]}" ;  INT_SIGN="${a[1]}" ;  CTYPE="${a[2]}" ;  KSIZE="${a[3]}" ;  REV_ACTION="${a[4]}" ;  REV_OCSP="${a[5]}" ; TYPE="rsa" ;
 if [[ "$KSIZE" = "384" || "$KSIZE" =  "521" || "$KSIZE" =  "256" ]]; then  TYPE="ec";
  else  TYPE="rsa" ;
 fi 

 #echo "DEBUG: SUBJ ${a[0]} -> INT_SIGN ${a[1]} -> CTYPE ${a[2]} -> KSIZE ${a[3]} -> REV_ACTION ${a[4]} -> TYPE rsa" ; enter2continue
 if [[ ! $INT_SIGN || ! $CTYPE || ! $KSIZE || ! $TYPE ]]; then correct_args ; fi
 
#Introduced this coz of Signing OSCP with RootCA
if [[ $INT_SIGN = "0" ]]; then
  CA_DIR="$BASE_DIR"
  #echo -n "DEBUG: Working DIR changted to $CA_DIR" ; enter2continue
  else
  CA_DIR="$BASE_DIR/intermediate$INT_SIGN"
fi

 #If there's a wrong method being called 
 if [[ $REV_ACTION != "O" && $REV_ACTION != "C" && $REV_ACTION != "S" && $REV_ACTION != "R" ]]; then echo "BIOHAZAAARD!! RUNNNNN!" ; exit ; fi    
 
 FNAME=$(echo ${a[0]}  | grep -o -P '(?<=CN=).*(?=/email)')
 #echo "DEBUG: Filename: $FNAME, REV_ACTION = $REV_ACTION"
 #FILE_KEY="$CA_DIR/private/$FNAME-$KSIZE.key.pem" 
 #FILE_CSR="$CA_DIR/csr/$FNAME-$KSIZE-$CTYPE.csr.pem" 
 #if [[ -f $FILE_KEY ]]; then  echo "CN=$FNAME: FILE_KEY or FILE_CSR already exits. Skip!!" ; continue ; fi #Check whether certificate already exists. 
 #if [[ $INT_SIGN == "0" ]]; then  INT_SIGN=""; fi  #Fixing issues with intermediate0 name scheme.

 if [[ $REV_ACTION == "C" ]]; then leaf_create_key ; create_csr ; sign_as_serv_cert #Server_cert Mode ; 
   cat $R_CERT >> $CA_DIR/certs/ca-cert-int$INT_SIGN.chain.cert.pem ;

 elif [[ $REV_ACTION == "O" ]]; then leaf_create_key ; create_csr ; sign_as_ocsp #OCSP_Mode
 elif [[ $REV_ACTION == "R" ]]; then revoke_cert #Revoke Mode
 elif [[ $REV_ACTION=='S' ]]; then skip_cert #Skip Cert
 else echo "!C !O !S .. it's a BioHazard!! RUNNNNN!" #exit
 fi  

done < $1
