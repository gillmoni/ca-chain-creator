#!/bin/bash
source ../cnf/ca_settings.txt

CERT_TYPE=$1
SIGNER_NO=$2

if [[ $1 == 'kill' ]]; then
	echo -n "Press ENTER to exit all running Openssl (ocsp) responders." ; enter2continue
	killall openssl
	did_it_work quit_OCSP_processes
	exit
fi

######################################################################################################
#AUTO DETECT PORT and RUN SERVER
######################################################################################################
if [[ ! $CERT_TYPE || ! $SIGNER_NO ]]; then echo "Not enough arguments. Exit!" ; echo "script: [leaf/int] [SIGNER_NO] [certificate]"; exit ; fi

CERT_TYPE=$1 ; SIGNER_NO=$2 ; END_CERT_TO_CHECK=$3

if [[ $CERT_TYPE = "int" ]]; then
	C_FILE="$CA_DIR/intermediate$SIGNER_NO/certs/intermediate$SIGNER_NO.cert.pem"
elif [[ $CERT_TYPE = "leaf" ]]; then
	if [[ $SIGNER_NO == '0' ]]; then
		C_FILE=$(find $CA_DIR/$BUILD/ | grep "test")
	else
		C_FILE=$(find $CA_DIR/intermediate$SIGNER_NO/$BUILD/ | grep "test")
	fi
fi

PORT=$(vc $C_FILE | grep OCSP | tail -c 5)
#did_it_work find_port
	
if [[ $DEBUG == '1' ]]; then
 #echo "DEBUG: FILE > $C_FILE" ; enter2continue
 echo -e "DEBUG: CERT > $CERT_TYPE > $C_FILE, and\nPORT > $PORT"
 echo Launching ./3.launch_OCSP_responder_and_client.sh $CERT_TYPE $SIGNER_NO $PORT.
fi

#Read OCSP_RESPONDER value from ca_settings.txt
case "$OCSP_RESPONDER" in
	"internal")	#echo -n "DEBUG: Using $OCSP_RESPONDER OCSP responder> " ; enter2continue 
				ROLE='server' ;
	;;
	"external")	#echo -n "DEBUG: Using $OCSP_RESPONDER OCSP responder> " ; enter2continue 
				ROLE='responder' ;
	;;
	*)			echo "Invalid Arguments> [OCSP_RESPONDER internal/external]" ; exit ;
	;;
 esac

./3.launch_OCSP_responder_and_client.sh $ROLE $CERT_TYPE $SIGNER_NO $PORT &
	
