#!/bin/bash
source ../cnf/ca_settings.txt

echo -n "Going Into ROOT/CA> " ; enter2continue
cd $CA_DIR ; 

echo -n "Press ENTER to check length of $CRT_FILE> " ; enter2continue
	vc $CA_DIR/$BUILD/$CRT_FILE | grep -i "len\|Info" -B1

echo -n "Press ENTER to check length of Intermediate 1> " ; enter2continue
	vc $CA_DIR/intermediate1/certs/intermediate1.cert.pem | grep -i "len\|Info" -B1

echo -n "Press ENTER to check length of Intermediate 2> " ; enter2continue
	vc $CA_DIR/intermediate2/certs/intermediate2.cert.pem | grep -i "len\|Info" -B1

echo -n "Press ENTER to check length of Intermediate 3> " ; enter2continue
	vc $CA_DIR/intermediate3/certs/intermediate3.cert.pem | grep -i "len\|Info" -B1