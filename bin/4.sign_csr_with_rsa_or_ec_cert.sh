#!/bin/bash
source ../cnf/ca_settings.txt
#############################################################################
# Create a Certificate Signing Request. Then have the root CA sign the User Certificate with their private key.
# A  user certificate is then created.
# Please refer to README.txt.
#
# Note that this script assumes that the following utilities are available:
#   - ssh, tftp, scp, openssl
# (c) 2015 Moni Gill for MCorp 
#
#############################################################################
#C_DEBUG="echo"
source ./ca_settings.txt
OPENSSL_CNF_DIR="$CA_DIR"
BASE_DIR="$CA_DIR"

#CA_RSA_KEY="$BASE_DIR/private/qe_rsa_ca.key.pem" ; CA_RSA_CERT="$BASE_DIR/certs/qe_rsa_ca.cert.pem "
#CA_EC_KEY="$BASE_DIR/private/qe_ec_ca.key.pem" ; CA_EC_CERT="$BASE_DIR/certs/qe_ec_ca.cert.pem"

CA_RSA_KEY="$BASE_DIR/private/ca.key.pem" ; CA_RSA_CERT="$BASE_DIR/certs/ca.cert.pem "
CA_EC_KEY="$BASE_DIR/private/ca.key.pem" ; CA_EC_CERT="$BASE_DIR/certs/ca.cert.pem"
#############################################################################
function enter2continue(){ 
  echo -n "Press ENTER to continue .... Ctrl+C to exit. " ; read input </dev/tty
}
#################################################################################################################
function sign_cert {   
 F_CERT="$CA_DIR/$F_CSR.$TYPE-$HALGO.cert.pem"
 if [[ $TYPE = "ec" ]]; then sign_cert_ec $F_CERT
 else sign_cert_rsa $F_CERT; fi


if [[ $TYPE = "ec" ]]; then 
  ADD_KEY_PART=$(grep -ozP '(?s)-----BEGIN EC PRIVATE KEY-----\n\K.*?(?=\n-----END EC PRIVATE KEY-----)' $F_CSR)
  echo "DEBUG: ADD_KEY_PART is $ADD_KEY_PART" ; enter2continue
  echo "-----BEGIN EC PRIVATE KEY-----" >> $F_CERT
  echo "$ADD_KEY_PART" >> $F_CERT
  echo "-----END EC PRIVATE KEY-----" >> $F_CERT

 else 
  ADD_KEY_PART=$(grep -ozP '(?s)-----BEGIN RSA PRIVATE KEY-----\n\K.*?(?=\n-----END RSA PRIVATE KEY-----)' $F_CSR)
  echo "DEBUG: ADD_KEY_PART is $ADD_KEY_PART" ; enter2continue
  echo "-----BEGIN RSA PRIVATE KEY-----" >> $F_CERT
  echo "$ADD_KEY_PART" >> $F_CERT
  echo "-----END RSA PRIVATE KEY-----" >> $F_CERT
 fi
 chmod 777 $F_CERT
 echo "FILE: Saved  $F_CERT, $(stat -c%s $F_CERT) bytes"
}
#################################################################################################################
function sign_cert_ec {  
 echo -n "DEBUG:Signing with EC> " ; enter2continue
 $C_DEBUG openssl ca -batch -passin pass:admin \
  -keyfile $CA_EC_KEY -cert $CA_EC_CERT \
  -config $OPENSSL_CNF_DIR/openssl.cnf \
  -extensions server_cert -notext -md $HALGO \
  -in $F_CSR -out $F_CERT
 echo -n "DEBUG:Completed Signing with EC> " ; enter2continue
}
#################################################################################################################
function sign_cert_rsa {  
  echo -n "DEBUG:Signing with RSA> " ; enter2continue
 $C_DEBUG openssl ca -batch -passin pass:admin \
  -keyfile $CA_RSA_KEY -cert $CA_RSA_CERT \
  -config $OPENSSL_CNF_DIR/openssl.cnf \
  -extensions server_cert -notext -md $HALGO \
  -in $F_CSR -out $F_CERT
 echo -n "DEBUG: Completed Signing with RSA> " ; enter2continue
}
#################################################################################################################
function verify_cert {
 F_NAME="$BASE_DIR/certs/$TYPE-$DEVICE-$KSIZE-$HALGO.cert.crt"
 openssl x509 -outform der -in $BASE_DIR/certs/$TYPE-$DEVICE-$KSIZE-$HALGO.cert.pem -out $BASE_DIR/certs/$TYPE-$DEVICE-$KSIZE-$HALGO.cert.crt
 if [[ "$4" = "rsa" ]]; then 
  openssl verify -CAfile $BASE_DIR/certs/rsa_qe_test_ca.cert.pem \
  $BASE_DIR/certs/$TYPE-$DEVICE-$KSIZE-$HALGO.cert.pem
 elif [[ "$4" = "ec" ]]; then
  openssl verify -CAfile $BASE_DIR/certs/qe_ecdsa_ca.cert.pem \
  $BASE_DIR/certs/$TYPE-$DEVICE-$KSIZE-$HALGO.cert.pem
 fi
 echo "FILE: Saved  $F_NAME, $(stat -c%s $F_NAME) bytes"
 echo "####################################################################################################################################################"
}
#################################################################################################################
function del_key_files { 
 D_CSR="$CA_DIR/csr/$TYPE-$DEVICE-$KSIZE.csr.pem"
 D_KEY="$CA_DIR/private/$TYPE-$DEVICE-$KSIZE.key.pem"
 D_CERT="$CA_DIR/certs/$TYPE-$DEVICE-$KSIZE-$HALGO.cert.pem"
 echo "DEBUG: Deleting $D_KEY> " ; echo "DEBUG: Deleting $D_CSR> " ; echo "DEBUG: Deleting $D_CERT> " ; enter2continue 
 rm $D_CSR $D_CERT $D_KEY
 echo -e "Done deleting." ; exit  
}
#################################################################################################################################################################
function revoke_cert(){ 
echo -n "DEBUG: I'm in recovation mode."; enter2continue
rev_file="$CA_DIR/index.txt" ; cnf_file="/usr/lib/ssl/openssl.cnf"
cat $rev_file | grep $DEVICE > revoke_list
while read rev_line; do
 B=($rev_line) ; REV_STATUS="${B[0]}" ;
 #echo "DEBUG: REV_STATUS = $REV_STATUS"
  if [[ $REV_STATUS == "R" ]]; then REV_ID="${B[3]}" ; echo "$REV_ID-> Already Revoked. Skipping!!" ; continue; fi
  if [[ $REV_STATUS == "V" ]]; then REV_ID="${B[2]}" ;
   #echo "DEBUG: Revoke File is <$rev_file>"
   echo "DEBUG: Certificate to be revoked is $REV_ID";
   $C_DEBUG openssl ca -config $cnf_file \
    -revoke /$BASE_DIR/newcerts/$REV_ID.pem \
    -batch -passin pass:admin
  fi      
done < revoke_list ; rm revoke_list
}
#################################################################################################################################################################
function fetch_root_$crtfile {
  
 if [[ $TYPE == "ec" ]]; then
  FILE="$CA_EC_CERT" ; T_FILE="$CA_DIR/$CRT_FILE.ec"
 elif [[ $TYPE == "rsa" ]]; then
  FILE="$CA_RSA_CERT" ; T_FILE="$CA_DIR/$CRT_FILE.rsa" 
 else
  echo "What to fetch [ec, rsa]> ";
  exit
 fi

 #echo "FILE -> $FILE"
 #cp $CA_DIR/certs/$FILE .
 #cp $FILE .
 cat $FILE >> $T_FILE
 #sudo rm $FILE
}
#################################################################################################################################################################
#################################################################################################################################################################
# CHANGE THE DEVICE NAME, AND SERIAL NUMBER. ENSURE COMMON NAME MATCHES DEVICE NAME
#DEVICE="$1" ; HALGO="$2" ; KSIZE="$3" ; TYPE="$4" ; ACTION="$5"
#if [[ ! $DEVICE || ! $HALGO ||  ! $KSIZE ||   ! $TYPE || ! $ACTION ]]; then
F_CSR="$1" ; TYPE="$3" ; HALGO="$2" ; ACTION="$4" ; 
if [[ ! $F_CSR || ! $HALGO || ! $TYPE || ! $ACTION ]]; then 
  echo "Arg 1, Enter CSR file to sign>" ;
  echo "Arg 2, Hashing algo (e.g md5, sha1, sha256, sha384, sha512)>" ;  
  echo "Arg 2, Type of CSR [rsa, ec]>" ; 
  echo "Arg 4, Action [s - Sign]>" ; 
  exit
fi

if [[ "$ACTION" = "s" ]]; then
  sign_cert;
  fetch_root_$crtfile
else
  echo "Exiting!! INVALID OPTION"
fi


if [[ "$5" == "s" ]]; then

  scp $F_CERT mgill@10.200.20.220:~/tmp/2015/Dec/
  scp ssl* mgill@10.200.20.220:~/tmp/2015/Dec/
fi
