#dcert /root/ca/certs/ca.cert.pem m > /dev/null
#dcert /root/ca/certs/ca.cert.pem c > /dev/null
source ../cnf/ca_settings.txt
echo -n "WARNING: Working in $CA_DIR. Press ENTER to continue> " ; enter2continue

if [[ ! $1 ]]; then
	echo -e "SYNTAX: ./script [create/c] [launch/l] [s/send/[x]] "
	echo -ne "Running with default parameters. Press ENTER to continue> "; enter2continue
fi

if [[ "$1" == "create" || $1 == 'c' ]]; then
rm -r $CA_DIR
#Create Base Chain
#-------------------
./1.create_root_intermediate_hierarchy.sh root rsa
./1.create_root_intermediate_hierarchy.sh int rsa 1 
./1.create_root_intermediate_hierarchy.sh int rsa 2 1
#./1.create_root_intermediate_hierarchy.sh int rsa 3 2
#./1.create_root_intermediate_hierarchy.sh int rsa 4 3
#./1.create_root_intermediate_hierarchy.sh int rsa 5 4
#./1.create_root_intermediate_hierarchy.sh int rsa 6 5


./1.create_root_intermediate_hierarchy.sh chain

./2.gen_leaf_certificates.sh $LEAF_CERT_CONFIG
fi

if [[ "$2" == "launch" || $2 == 'l' || $1 == 'l' ]]; then
 #Launch all OCSP responders.
 #-------------------
 echo -n "Launching all OCSP responders>" ; enter2continue
 ./8.auto-detect-nodes.sh int 1
 ./8.auto-detect-nodes.sh int 2
 #./8.auto-detect-nodes.sh int 3
 #./8.auto-detect-nodes.sh int 4
 #./8.auto-detect-nodes.sh int 5
 #./8.auto-detect-nodes.sh int 6

 ./8.auto-detect-nodes.sh leaf 1
 ./8.auto-detect-nodes.sh leaf 2
 #./8.auto-detect-nodes.sh leaf 3
 #./8.auto-detect-nodes.sh leaf 4
 #./8.auto-detect-nodes.sh leaf 5
 #./8.auto-detect-nodes.sh leaf 6

fi

if [[ $3 == 's' || $3 == 'send' ]]; then
 echo "I'll send $CRT_FILE to $BUILD on 192.168.0.$2";
 sf p $4 '/root/ca/$BUILD/$CRT_FILE' $CRT_FILE
 echo "Send CA-Chain cert!! Bye!!"
fi

chmod 0777 $CA_DIR/* -R
#acert /root/ca/certs/ca.cert.pem m > /dev/null
#acert /root/ca/certs/ca.cert.pem c > /dev/null
